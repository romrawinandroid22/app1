package com.romrawin.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView


class HelloActivity : AppCompatActivity() {
    var showname: TextView? =null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)
        showname =findViewById<TextView>(R.id.Nametxt)
        var intent = intent
        showname!!.text = intent.getStringExtra("Nametxt")
        supportActionBar!!.title= "HELLO"
    }
}