package com.romrawin.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    companion object {
        private const val TAG = "MainActivity"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var button1 = findViewById<Button>(R.id.button2)
        button1.setOnClickListener{
            val intent = Intent (this,HelloActivity::class.java)
            intent.putExtra("Nametxt","Romrawin Inchai")
            startActivity(intent)
        }
        val nameTextView: TextView = findViewById(R.id.textView)
        val idTextView: TextView = findViewById(R.id.textView2)
        Log.d(TAG,""+nameTextView.text)
        Log.d(TAG,""+idTextView.text)
        supportActionBar!!.title= "HELLO"
    }
}